<?php

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use \PhpOffice\PhpSpreadsheet\Cell\Coordinate;

$lastVersion = filemtime("sheet.xlsx");

$force = isset($_GET['force']) ? $_GET['force'] : false;

if ($force || (filemtime('data.txt') < $lastVersion)) {
	$reader = new Xlsx($spreadsheet);
	$spreadsheet = $reader->load("sheet.xlsx");
	$worksheet = $spreadsheet->getActiveSheet();

	$highestRow = $worksheet->getHighestRow();
	$highestColumn = $worksheet->getHighestColumn();
	$highestColumnIndex = Coordinate::columnIndexFromString($highestColumn);

  $firstRow = 2;
  $pathologyIndex = 1;
  $themeIndexStart = 2;
  $themeIndexEnd = 7;
  $organisationIndex = 8;


	$pathologies = [];
	$themes = [];
	$data = [];

	$data = [];
	for ($row = $firstRow; $row <= $highestRow; $row++) {
		$pathology    = $worksheet->getCellByColumnAndRow($pathologyIndex, $row)->getValue();
		$pathology    = strtolower(trim($pathology));
		$organisation = $worksheet->getCellByColumnAndRow($organisationIndex, $row)->getValue();
		$organisation = strtolower(trim($organisation));

    $themes = [];
    for($i = $themeIndexStart; $i <= $themeIndexEnd; $i++) {
  		$temp        = $worksheet->getCellByColumnAndRow($i, $row)->getValue();
  		$temp        = strtolower(trim($temp));
      if ($temp) {
        $themes[] = $temp;
      }
    }

		foreach ($themes as $theme) {
			$theme = trim($theme);
			if ($theme) {
				$data[] = [
					'pathology'   => $pathology,
					'organisation' => $organisation,
					'theme'        => $theme,
				];
			}
		}
	}

	$data = array_merge($data);
	foreach($data as $index => $dataValue) {
		foreach ($data as $indexToCheck => $dataToCheck) {
			if ($index == $indexToCheck) {
				continue;
			}
			if (
				($dataToCheck['pathology'] == $dataValue['pathology']) &&
				($dataToCheck['organisation'] == $dataValue['organisation']) &&
				($dataToCheck['theme'] == $dataValue['theme'])
			) {
				unset($data[$index]);
			}
		}
	}

	$data = serialize($data);
	file_put_contents('data.txt', $data);
	$themes = [];
}

$data = file_get_contents('data.txt');
$data = unserialize($data);

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
	<title>Moteur de recherche HEDS</title>
	<meta name="description" content="Prototype de moteur de recherche pour HEDS">
	<link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
	<!-- <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/light.css'> -->
	<!-- <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.css'> -->
	<style type="text/css" media="screen"></style>

	<!-- development version, includes helpful console warnings -->
	<!-- <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script> -->
	<!-- production version, optimized for size and speed -->
	<script src="https://cdn.jsdelivr.net/npm/vue@2"></script>

</head>
<body class="max-w-6xl m-auto">
	<div id="wrapper" class="flex flex-col justify-center">
		<div id="header" class="p-5 border-b-2 border-solid">
			<h1 class="text-6xl">Moteur de recherche HEDS</h1>
			<p>Prototype de moteur de recherche pour HEDS</p>
		</div>
		<div id="app-heds">
			<form class="bg-white shadow-md rounded px-8 pt-10 pb-8 mb-4" v-on:submit="cancelEvent($event)">
				<div class="mb-4">
					<div class="w-1/2 inline-block pl-1 pr-1">
						<label class="block text-gray-700 text-sm font-bold mb-2" for="pathology">
							Rechercher par famille de pathologies...
						</label>
						<select id="pathology" class="block w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline" v-on:change="searchChanged($event)" v-model="searchPathology">
							<option :value="null" class="text-gray-500" disabled>Filtrer par pathologies</option>
							<option value="">-- Toutes les pathologies</option>
							<option v-for="pathologie in pathologies" :value="pathologie">{{pathologie}}</option>
						</select>
						<div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
							<svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
						</div>
					</div><div class="w-1/2 inline-block  pl-2">
						<label class="block text-gray-700 text-sm font-bold mb-2" for="organisation">
							... ou par organisation
						</label>
						<select id="organisation" class="block w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline" v-on:change="searchChanged($event)" v-model="searchOrganisation">
							<option :value="null" class="text-gray-500" disabled>Filtrer par organisation</option>
							<option value="">-- Toutes les organisations</option>
							<option v-for="organisation in currentOrganisations" :value="organisation">{{organisation}}</option>
						</select>
						<div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
							<svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
						</div>
					</div>
				</div>
				<template v-if="currentThemes.size">
					<hr/>
					<label class="mt-2 block text-gray-700 text-sm font-bold mb-2" for="theme">
						<button class="border rounded p-2 text-gray-700 text-lg font-bold" v-on:click="openResults()">🔍 Cliquer ici pour voir toutes les infos liées aux thèmes correspondants aux résultats de la recherche</button>...
					</label>

					<label class="block text-gray-700 text-sm font-bold mb-2" for="theme"><div class="inline-block p-1">
						... ou cliquer sur le thème pour préciser votre recherche
					</div>
				</label>

				<div id="theme-list" class="flex flex-row flex-wrap justify-start">
					<span v-for="(theme, index) in currentThemes" :key="index" class="cursor-pointer shadow border rounded p-1 mr-5 mb-2" :class="{'bg-green-400': hoverTheme && (theme.indexOf(hoverTheme) > -1)}" v-on:mouseover="selectTheme(theme)" v-on:mouseleave="selectTheme()" v-on:click="openResults(theme)">{{theme}}</span>
				</div>
			</template>
			<template v-if="(searchTheme || searchOrganisation || searchPathology) && !currentThemes.size">
				<span class="italic">Votre recherche n'a donné aucun résultat</span>
			</template>
		</form>
	</div>
</div>

<script>
	var appHeds = new Vue({
		el: '#app-heds',
		data: {
			hoverTheme: null,
			force: '<?php echo $force; ?>',
			timeoutSelect: null,
			keySearch: 0,
			searchTheme: null,
			searchOrganisation: null,
			searchPathology: null,
			message: 'You loaded this page on ' + new Date().toLocaleString(),
			pathologies: [
			<?php
			foreach ($data as $index => $dataValue) {
				$string = $dataValue['pathology'];
				$string = strtr($string, array_flip(get_html_translation_table(HTML_ENTITIES, ENT_QUOTES)));
				$string = str_replace(chr(0xC2).chr(0xA0), ' ', $string);
				if (!$string) {
					continue;
				}
				echo '"'.trim($string).'"';
				if ($index != (sizeof($themes) - 1)) echo ",";
			}
			?>
			],
			organisations: [
			<?php
			foreach ($data as $index => $dataValue) {
				$string = $dataValue['organisation'];
				$string = strtr($string, array_flip(get_html_translation_table(HTML_ENTITIES, ENT_QUOTES)));
				$string = str_replace(chr(0xC2).chr(0xA0), ' ', $string);
				if (!$string) {
					continue;
				}
				echo '"'.trim($string).'"';
				if ($index != (sizeof($themes) - 1)) echo ",";
			}
			?>
			],
			data: [
			<?php

			foreach ($data as $index => $dataValue) {
				$test = trim(implode('', $dataValue));
				if (!$test) {
					continue;
				}
				echo '{';
				echo '"organisation": "'.$dataValue['organisation'].'",';
				echo '"theme": "'.$dataValue['theme'].'",';
				echo '"pathology": "'.$dataValue['pathology'].'",';
				echo '"id": "'.$index.'"';
				echo '}';
				if ($index != (sizeof($themes) - 1)) echo ",";
			}
			?>
			],
			currentThemes: [],
			currentOrganisations: [],
		},
		created() {
			this.pathologies = new Set(this.pathologies);
			this.currentOrganisations = new Set(this.organisations);
		},
		methods: {
			selectTheme: function(theme = null) {
				this.hoverTheme = theme;
	        },
	        searchChanged: function() {
	        	this.currentThemes = [];
	        	this.currentOrganisations = [];
	        	let tempFoundThemes = [];
	        	let tempFoundOrganisations = [];
	        	this.searchTheme        = this.searchTheme ? this.searchTheme.toLowerCase() : '';
	        	this.searchOrganisation = this.searchOrganisation ? this.searchOrganisation.toLowerCase() : '';
	        	this.searchPathology    = this.searchPathology ? this.searchPathology.toLowerCase() : '';

	        	for(let i = 0; i < this.data.length; i++) {
	        		if (
	        			(this.data[i].theme.search(this.searchTheme) != -1) &&
	        			(this.data[i].pathology.search(this.searchPathology) != -1)
	        			) {
	        			tempFoundOrganisations.push(this.data[i].organisation);
		        	}

	        		if (
	        			(this.data[i].theme.search(this.searchTheme) != -1) &&
	        			(this.data[i].organisation.search(this.searchOrganisation) != -1) &&
	        			(this.data[i].pathology.search(this.searchPathology) != -1)
	        			) {
	        			tempFoundThemes.push(this.data[i].theme);
	        			tempFoundOrganisations.push(this.data[i].organisation);
		        	}
		        }

	        tempFoundThemes.sort((a, b) => {
	        	if (a < b)
	        		return -1;
	        	if (a > b)
	        		return 1;
	        	return 0;
	        });
	        this.currentThemes = new Set(tempFoundThemes);

	        tempFoundOrganisations.sort((a, b) => {
	        	if (a < b)
	        		return -1;
	        	if (a > b)
	        		return 1;
	        	return 0;
	        });
	        this.currentOrganisations = new Set(tempFoundOrganisations);
	        if (this.searchOrganisation && !this.currentOrganisations.has(this.searchOrganisation)) {
	        	this.searchOrganisation = null;
	        	this.searchChanged();
	        }
	    },
	    openResults: function(theme = '') {
			window.open('results.php?theme='+theme+'&pathology='+this.searchPathology+'&organisation='+this.searchOrganisation+'&force='+this.force);
	    },
	    cancelEvent: function(event) {
	    	event.preventDefault();
	    },
	}
})
</script>
</body>
</html>
